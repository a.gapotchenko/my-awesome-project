from spellchecker import SpellChecker
import pymorphy2
import jamspell
from natasha import (
    Segmenter,
    MorphVocab,
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    Doc
)

# Инициализация компонентов библиотеки
segmenter = Segmenter()
morph_vocab = MorphVocab()
emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)
syntax_parser = NewsSyntaxParser(emb)
jsp = jamspell.TSpellCorrector()
assert jsp.LoadLangModel('ru_small.bin')


def load_text(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        return f.read()


def save_text(file_path, text):
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(text)


def correct_spelling(text, strip):
    doc = Doc(text)
    doc.segment(segmenter)
    doc.tag_morph(morph_tagger)
    doc.parse_syntax(syntax_parser)

    for token in doc.tokens:
        token.lemmatize(morph_vocab)

    corrected_tokens = [token for token in doc.tokens]
    corrected_text = ''
    for i, token in enumerate(corrected_tokens):
        if token.pos != 'PUNCT':
            corrected_text += token.text + ' '
        else:
            corrected_text += token.text
    return strip.FixFragment(text)

def spelling(text):
    spell = SpellChecker(language='ru')
    morph = pymorphy2.MorphAnalyzer()
    words = text.split()
    corrected_words = []

    for word in words:
        if not spell.known([word]):
            # Получаем исправление для слова
            corrected_word = spell.correction(word)
            if corrected_word:
                # Анализируем исходное слово
                parsed_word = morph.parse(word)[0]
                # Применяем исправление, сохраняя исходные падежи и окончания
                corrected_word = parsed_word.inflect({parsed_word.tag.case}).word
            else:
                # Если исправление не найдено, оставляем исходное слово
                corrected_word = word
        else:
            corrected_word = word
        corrected_words.append(corrected_word)

    return ' '.join(corrected_words)

def main():
    input_file = 'text.txt'            # Имя файла с текстом для проверки
    output_file = 'corrected_text.txt'  # Имя файла для сохранения исправленного текста

    text = load_text(input_file)
    corrected_text = correct_spelling(text, jsp)
    revised_text = spelling(corrected_text)
    save_text(output_file, corrected_text)

    print(f"Исправленный текст был сохранен в файле '{output_file}'.")


if __name__ == '__main__':
    main()
